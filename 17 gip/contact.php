<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <title>contact</title>
  <meta name="description" content="Website over Atletiek voor gip 2015-2016 6 Mul a">
  <meta name="author" content="Ilias Taeymans">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">


</head>

<body>
     <div class="container">
    
        <div class="row">
          <div class="twelve.columns" id="header">
            <img src="images/header.png" alt="header" />
          </div>
        </div>
    
        <div class="row">
          <div class="twelve.columns">
            <nav>
              <ul id="navlist">
                <li><img src="images/logo.png" alt="logo" id="logoimg" /></li>
                <li><a href="index.php">HOME</a></li>
                <li><a href="sporten.php">SPORTEN</a></li>
                <li><a href="contact.php">CONTACT</a></li>
              </ul>
            </nav>
          </div>
        </div>
        
     <div class="row">
     <div class="twelve.columns" id="contact">
     <h4> Contacteer Me </h4>
     <p class="centerp2"> Ik ben Ilias taeijmans ik doe multimedia in het zesde middelbaar op stedelijk lyceum cadix. 
		 Dit is mijn eindproject, heb je enige vragen log dan even in en stel ze gerust.
		 Schrijf je ook in voor wedstrijden op de kalender als je zin hebt.</p>
         
         <form>
          <fieldset>
         <label for="naam">Naam</label>
         <input type="text" name="naam" id="naam" placeholder="naam" maxlength="30" required>
         </fieldset>
         
          <fieldset>
         <label for="geboortedatum">Geboortedatum</label>
         <input type="date" name="geboortedatum" id="geboortedatum">
         </fieldset>
            
         <fieldset>  
         <label for="email">E-mail</label>
         <input type="email" name="email" id="email" maxlength="100" placeholder="uw mailadres" required>
         </fieldset>
         
         <fieldset>
         <label for="opmerkingen">opmerkingen</label>
         <textarea name="opmerkingen" id="opmerkingen" placeholder="opmerkingen"   ></textarea>  
      
         </fieldset> 
         <fieldset> 
           <input type="submit" name="submit" id="submit" value="verstuur" >
           </fieldset>
        
    </form> 
         

     </div></div>
     
     
     
        
        
   
</div>
</body>
</html>        