<?php
		$recordsID = "";
		$naam = "";
		$discipline = "";
		$recordtijden = "";
	require_once("includes/dbconnect.inc.php");
	require_once("includes/functions.inc.php");
		$qryRecords = 
		"SELECT recordsID,  naam, discipline, recordtijden 
		from tblrecords 
		Order by naam ASC";
		
		if($stmt = mysqli_prepare($dbconnect, $qryRecords)) {
		
		mysqli_stmt_execute($stmt);	
		mysqli_stmt_bind_result($stmt, $recordsID, $naam, $discipline, $recordtijden);	
		mysqli_stmt_store_result($stmt);
		$aantal = mysqli_stmt_num_rows($stmt);
		mysqli_close($dbconnect);
	}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Athlete's Quarter</title>
  <meta name="description" content="Website over Atletiek voor gip 2015-2016 6 Mul a">
  <meta name="author" content="Ilias Taeymans">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="js/slider/engine1/style.css" />
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">






</head>

<body>
  <div class="container">

    <div class="row">
      <div class="twelve.columns" id="header">
        <img src="images/header.png" alt="header" />
      </div>
    </div>

    <div class="row">
      <div class="twelve.columns">
        <nav>
          <ul id="navlist">
            <li><img src="images/logo.png" alt="logo" id="logoimg" /></li>
            <li><a href="index.php">HOME</a></li>
            <li><a href="sporten.php">SPORTEN</a></li>
            <li><a href="contact.php">CONTACT</a></li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="row" id="main">
      <div class="nine.columns" id="slidespace">


        <!-- Start WOWSlider.com BODY section -->
        <div id="wowslider-container1">
          <div class="ws_images">
            <ul>
              <li><img src="js/slider/data1/images/slide1.jpg" alt="Winnaars belofte bk" title="Winnaars belofte bk" id="wows1_0" /></li>
              <li>
                <img src="js/slider/data1/images/slide2.jpg" alt="responsive slider" title="start finale 200m" id="wows1_1" />
              </li>
              <li><img src="js/slider/data1/images/slide3.jpg" alt="Winnaars junioren" title="Winnaars junioren" id="wows1_2" /></li>
            </ul>
          </div>
          <div class="ws_bullets">
            <div>
              <a href="#" title="Winnaars belofte bk"><span><img src="js/slider/data1/tooltips/slide1.jpg" alt="Winnaars belofte bk"/>1</span></a>
              <a href="#" title="start finale 200m"><span><img src="js/slider/data1/tooltips/slide2.jpg" alt="start finale 200m"/>2</span></a>
              <a href="#" title="Winnaars junioren"><span><img src="js/slider/data1/tooltips/slide3.jpg" alt="Winnaars junioren"/>3</span></a>
            </div>
          </div>
        </div>
      </div>

      <div class="three.columns" id="article1">
        <div id="titlespace">
          <h4 class="bold">Het Laatste Nieuws</h4>
        </div>
        <img src="images/foto1_t.jpg" alt="foto1" />
        <p class="centerp">
          Dylan op de 60 meter 2de plaats.
        </p>
        <img src="images/foto2_t.jpg" alt="foto1" />
        <p class="centerp">
          Olse na hun overwinning in de finale.
        </p>

        <img src="images/foto3_t.jpg" alt="foto1" />
        <p class="centerp">
          VAC sprint letterlijk naar de overwinning.
        </p>
        <div id="titlespace2">
          <h4 class="bold">Records</h4>
        </div>
        
       <table>
        <div class="table">
<?php
        
        while (mysqli_stmt_fetch($stmt)) {
		echo "<tr>" ;
		echo "<td>" . $naam . "</td>";
		echo "<td>" . $discipline .  "</td>";	
		echo "</tr>";
		echo "<tr>";
		echo "<td>" . $recordtijden .  "</td>";
		echo "</tr>";


	}
	mysqli_stmt_close($stmt);
?>   
      <!--  <p class="centerp">
         "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi vero optio nesciunt facere quod, labore cumque, vitae recus
         andae sint iste asperiores aut earum! Voluptatem ex laborum sapiente fuga maiores ullam rem a saepe quis natus, blanditiis maxime.
        </p> -->
        </div>
       </table>

      </div>

      <div class="row">
        <div class="nine.columns" id="news1">
          <h5 class="mleft2">Een zegen voor het team</h5>
          <p class="mleft1">
          
          Knap resultaat op het belgisch kampioenschap indoor, hier sluiten we ook het indoor seizoen mee af...
           

          </p>
          
          <img src="images/foto4.jpg" alt="foto4" id="news1img" />
          <button id="news1button">Lees Meer >></button>
          <div id="leesmeer1">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non quia commodi fuga ipsum obcaecati quisquam eum unde ad ipsa. Modi eaque consequatur dolorem illo perspiciatis, corporis laborum vel veritatis, eius, cumque quae excepturi temporibus iure rerum
              facere voluptatibus obcaecati molestias? Eum ab autem aut iure impedit adipisci quidem maiores repudiandae non ad quisquam atque iste, voluptate delectus animi vero neque optio, quis reiciendis ex unde minus officiis magni. Quidem obcaecati
              magnam pariatur sit! Ipsa officia exercitationem quis quasi, quo eveniet aliquid qui deleniti, facere veritatis adipisci vel beatae iure maxime! Quo quis numquam aut consectetur, omnis, unde. Magnam, itaque, sunt?
            </p>
          </div>
        </div>
        <div class="offset-by-three.columns">

        </div>

      </div>


    </div>
  </div>
  <script src="js/slider/engine1/jquery.js "></script>
  <script src="js/slider/engine1/wowslider.js "></script>
  <script src="js/slider/engine1/script.js "></script>
  <script src="js/popout.js"></script>
</body>

</html>
