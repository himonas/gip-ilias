<?php 
	require_once("includes/dbconnect.inc.php");
	//kijken of er een id is meegegeven in de url
	if (isset($_GET["rid"])) {
		$recordsID = $_GET["rid"];
		$qrySelectrecords = 
			"SELECT naam
				FROM tblrecords
				WHERE recordsID = ?";
		if($stmt = mysqli_prepare($dbconnect, $qrySelectRecords)) {
			mysqli_stmt_bind_param($stmt, "i", $recordsID);
			mysqli_stmt_execute($stmt);
			mysqli_stmt_bind_result($stmt, $naam);
			mysqli_stmt_fetch($stmt);
			mysqli_stmt_close($stmt);
		}
	} elseif (isset($_POST["recordsID"])) {
		//is er bevestigd via het formulier ?
		$recordsID = $_POST["recordsID"];
		
		$qryDeleteRecords = "DELETE FROM tblrecords WHERE recordsID = ?";
		
		if($stmt = mysqli_prepare($dbconnect, $qryDeleteRecords)) {
			mysqli_stmt_bind_param($stmt, "i", $recordsID);
			if(mysqli_stmt_execute($stmt)) {
				//succesvol verwijderd
				
						$feedback = "<p>Record succesvol verwijderd</p>";		
			} else {
						//query niet succesvol uitgevoerd	
						$feedback = "<p>Er heeft zich een probleem voorgedaan</p>";
						$feedback .= mysqli_stmt_error($stmt);
			} //if($mysqli_stmt_execute($stmt 


		} //if($stmt 

		
	} else {
		$feedback = "<p>U hebt geen record geselecteerd</p>";	
	}
	
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Verwijder records</title>
<link href="reset.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="container">
<?php
	if(isset($feedback)) {
		echo $feedback;	
	}
	
	if(isset($naam)) {
?>
        <form id="verwijderrecords" name="verwijderrecords" method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
            <p>Bent u zeker dat u volgende record wilt verwijderen ?</p>
            <p><?php echo $naam;?></p>
            <input type="hidden" name="recordsID" value="<?php echo $recordsID;?>">
            <input type="submit" name="submit" id="submit" value="Bevestig">
        </form>
<?php
	}
?>        
</div>
</body>
</html>