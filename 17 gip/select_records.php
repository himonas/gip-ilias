<?php 
	/*session_start();
	print_r($_SESSION);
    if(!isset($_SESSION["gipUser"])){
		//niet ingelogd
			header("Location: login.php");
	}elseif($_SESSION["gipRecht"] != 5){
		//admin heeft rechten 5
		header("Location:)index.php");
		die();
	}  */
	
	require_once("includes/dbconnect.inc.php");
	//query
		$qrySelectRecords = 
		"SELECT recordsID,  naam, discipline, recordtijden  
		from tblrecords
		Order by naam ASC";
	// query voorbereiden : kijken of die correct is	
	if($stmt = mysqli_prepare($dbconnect, $qrySelectRecords)) {
		//query uitvoeren
		mysqli_stmt_execute($stmt);
		//variabelen opgeven waarin de waarden worden gestopt
		mysqli_stmt_bind_result($stmt, $recordsID, $naam, $discipline, $recordtijden);
		//wanneer je meerdere records ku nt terug krijgen moet het resultat opgeslagen worden om later te doorlopen
		mysqli_stmt_store_result($stmt);
		//het aantal records inde resultset 
		$aantal = mysqli_stmt_num_rows($stmt);
		// connctie met de server sluiten indien er niet meer met de database gedaan moet worden
		mysqli_close($dbconnect);
	}
?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>select_records</title>
</head>

<body>
	<div class="container">
		<h1>overzicht records</h1>
        <p>we hebben <?php echo $aantal;?> records in onze database.</p>
        
        <table>
        
<?php
	
	while (mysqli_stmt_fetch($stmt)) {
		echo "<tr>" ;
		echo "<td>" . $naam . "</td>";
		echo "<td>" . $discipline .  "</td>";	
		echo "<td>" . $recordtijden .  "</td>";
		echo "<td><a href='insert_records.php?kid=" . $recordsID . "'> aanpassen </a></td>";
		echo "</tr>";
		echo "<td><a href='delete_records.php?kid=" . $recordsID . "'> verwijder </a></td>";
		echo "</tr>";
	}
	mysqli_stmt_close($stmt);
?>        
</table>
    </div>
</body>
</html>