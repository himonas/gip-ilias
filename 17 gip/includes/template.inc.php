<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $paginatitel; ?></title>
</head>

<body>
<div class="container"> 
   	  <nav>
    	<ul>
        
        	<li><a href="select_records.php">overzicht records</a></li>
            <li><a href="insert_records.php">records toevoegen</a></li>
        </ul>
      </nav>
      <main>
      		<?php 
				echo $inhoud; 
			?>	
      </main>
      <footer>
   	  		<p>&copy; SLC</p>
      </footer> 
</div>     
</body>
</html>