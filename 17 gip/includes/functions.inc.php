<?php
	//functie die vergelijkt of 2 paswoorden gelijk zijn
	//vervolgens wordt er gecontroleerd of het paswoord een minimumlengte heeft
	//als laatste wordt er een sha1-encryptie terug gegeven
	//indien paswoorden niet gelijk zijn wordt "fout" teruggegeven
	//indien het paswoord niet aan de minimumlengte voldoet wordt "kort" teruggegeven  
	function prepPass ($password1, $password2) {
		$minLength = 4; //minimumlengte
		//controleren of de 2 paswoorden gelijk zijn
		if ($password1 == $password2) {
			//controleren of het paswoord een minimumlengte heeft
			if (strlen($password1) >= $minLength) {
				//sha1-encryptie van het paswoord
				$result = sha1($password1);
			} else {
				//niet lang genoeg
				$result = "kort";	
			}
		} else {
			//niet gelijk
			$result = "fout";	
		}
		return $result; //$result teruggeven
	}
	
	function encryptPass($paswoord) {
		$paswoord = sha1($paswoord);
		return $paswoord;
	}
	
	//functie die het volgende uitvoert op de gebruikersnaam :
	//verwijder spaties voor en achter
	//kijk of gebruikersnaam minimumlengte heeft, indien niet wordt "kort" teruggegeven
	//converteer naar kleine letters
	function prepUser ($username) {
		$minLength = 5; //minimumlengte
		$username = trim($username); //verwijder spaties voor en achter
		//controleren of de username een minimumlengte heeft
		if (strlen($username) >= $minLength) {
			$result = strtolower($username); //converteert naar kleine letters
		} else {
			$result = "kort";	
		}
		return $result; //waarde teruggeven
	}
	
	//functie die in een tekst line breaks vervangt door br-tags
	//speciale karakters omzet naar hun html-code
	function prepText($text) {
		$text = nl2br($text); //line breaks
		$text = htmlentities($text); //speciale karakters
		return $text;
	}


	//date2string : een datum omzetten naar een string.  Deze functie gaat ervan uit dat de datum in Nederlands formaat werd ingevoerd dd-mm-jjjj
	function date2string($date) {
		//settings omzetten naar Belgische instellingen
		setlocale(LC_ALL, "nl_BE");
		$arrDate = explode("/", $date);
		$date = $arrDate[2] . '/' . $arrDate[1] . '/' . $arrDate[0];
		//string omzetten naar UNIX timestamp
		$date = strtotime($date);
		//timestamp omzetten naar een string 9 oktober 2013
		$date = strftime("%e %B %Y", $date);
		return $date;
	}

	//datediff : berekent het verschil tussen 2 data
	function datediff($date1, $date2){
		//date1 omzetten naar UNIX timestamp
		$arrDate1 = explode("/", $date1);
		$date1 = $arrDate1[2] . '/' . $arrDate1[1] . '/' . $arrDate1[0];
		$date1 = strtotime($date1);
		//date2 omzetten naar UNIX timestamp
		$arrDate2 = explode("/", $date2);
		$date2 = $arrDate2[2] . '/' . $arrDate2[1] . '/' . $arrDate2[0];
		$date2 = strtotime($date2);

		$datediff = $date1 - $date2;
		$datediff = floor($datediff/(60*60*24)); //omzetten naar dagen
		return $datediff;
	}	
?>
