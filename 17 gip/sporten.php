<?php
		$recordsID = "";
		$naam = "";
		$discipline = "";
		$recordtijden = "";
	require_once("includes/dbconnect.inc.php");
	require_once("includes/functions.inc.php");
		$qryRecords = 
		"SELECT recordsID,  naam, discipline, recordtijden 
		from tblrecords 
		Order by naam ASC";
		
		if($stmt = mysqli_prepare($dbconnect, $qryRecords)) {
		
		mysqli_stmt_execute($stmt);	
		mysqli_stmt_bind_result($stmt, $recordsID, $naam, $discipline, $recordtijden);	
		mysqli_stmt_store_result($stmt);
		$aantal = mysqli_stmt_num_rows($stmt);
		mysqli_close($dbconnect);
	}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Athlete's Quarter</title>
  <meta name="description" content="Website over Atletiek voor gip 2015-2016 6 Mul a">
  <meta name="author" content="Ilias Taeymans">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="js/slider/engine1/style.css" />
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">






</head>

<body>
  <div class="container">

    <div class="row">
      <div class="twelve.columns" id="header">
        <img src="images/header.png" alt="header" />
      </div>
    </div>

    <div class="row">
      <div class="twelve.columns">
        <nav>
          <ul id="navlist">
            <li><img src="images/logo.png" alt="logo" id="logoimg" /></li>
            <li><a href="index.php">HOME</a></li>
            <li><a href="sporten.php">SPORTEN</a></li>
            <li><a href="contact.php">CONTACT</a></li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="row" id="main">
      <div class="nine.columns" id="slidespace">
      <img src="images/foto_promo2.jpg" alt="promovid"/>

      </div>

      <div class="three.columns" id="article1">
        <div id="titlespace">
          <h4 class="bold">Het Laatste Nieuws</h4>
        </div>
        <img src="images/foto1_t.jpg" alt="foto1" />
        <p class="centerp">
          Dylan op de 60 meter 2de plaats.
        </p>
        <img src="images/foto2_t.jpg" alt="foto1" />
        <p class="centerp">
          Olse na hun overwinning in de finale.
        </p>

        <img src="images/foto3_t.jpg" alt="foto1" />
        <p class="centerp">
          VAC sprint letterlijk naar de overwinning.
        </p>
        <div id="titlespace2">
          <h4 class="bold">Records</h4>
        </div>
        
       <table>
        <div class="table">
<?php
        
        while (mysqli_stmt_fetch($stmt)) {
		echo "<tr>" ;
		echo "<td>" . $naam . "</td>";
		echo "<td>" . $discipline .  "</td>";	
		echo "</tr>";
		echo "<tr>";
		echo "<td>" . $recordtijden .  "</td>";
		echo "</tr>";


	}
	mysqli_stmt_close($stmt);
?>   
      <!--  <p class="centerp">
         "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi vero optio nesciunt facere quod, labore cumque, vitae recus
         andae sint iste asperiores aut earum! Voluptatem ex laborum sapiente fuga maiores ullam rem a saepe quis natus, blanditiis maxime.
        </p> -->
        </div>
       </table>



      
  </div>
  <script src="js/slider/engine1/jquery.js "></script>
  <script src="js/slider/engine1/wowslider.js "></script>
  <script src="js/slider/engine1/script.js "></script>
  <script src="js/popout.js"></script>
</body>

</html>
