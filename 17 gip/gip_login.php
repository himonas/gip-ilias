<?php 
session_start();

require_once("includes/dbconnect.inc.php");
require_once("includes/functions.inc.php");

$paginatitel = "login";
$inhoud = "";
$succes = false;

	/***************************************************************************************************/
	/*                              verwerking                                                         */
	/***************************************************************************************************/

if($_SERVER['REQUEST_METHOD'] == "POST"){
	$gebruikersnaam = $_POST["gebruikersnaam"];
	$paswoord = $_POST["paswoord"];
	$paswoord = encryptPass($paswoord);
	
	$qryCheckUser = "SELECT rechten, gebruikersID
						FROM tblgebruikers
						WHERE gebruikersnaam LIKE ? AND paswoord LIKE ?";
						
	if($stmt = mysqli_prepare($dbconnect, $qryCheckUser)){
		mysqli_stmt_bind_param($stmt, "ss", $gebruikersnaam, $paswoord);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $rechten, $gebruikersID);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
		
	}
	if(!empty($gebruikersID)){
		//logingegevens kloppen
		//we kunnen nu de gegevens opslaan in een sessie. de naam van zo'n sessie variabelen kies je zelf
		$_SESSION["gipUser"] = $gebruikersnaam;
		$_SESSION["gipRecht"] = $rechten;
		$succes = true;
		$feedback = "welkom" . $gebruikersnaam . ". U gaat nu opniew naar de beginpagina .";
		
		// eventuaal kan je ook redirect dopen naar een bepaalde pagina diet werkt enkel indien er nog niets naar de nrower verstuurdwerd.
		// header("location:index.php");
		// of met een vertraging zodat je een bericht kunt tonen
		header("refresh: 2; url=index.php");
	} else  {
		//gebruikersnaam of paswoord zijn foutief ingevoerd 
		$feedback = "de gebruikersnaam of het paswoord was niet correct";
	
	}
						
}
mysqli_close($dbconnect);
	/***************************************************************************************************/
	/*                               eindeverwerking                                                   */
	/***************************************************************************************************/
	
	
	
	
	
	


	/***************************************************************************************************/
	/*                               opbouwregistratie form                                             */
	/***************************************************************************************************/
if($succes) { 
	echo '<p>' . $feedback . '</p>';
} else {
	if(isset($feedback)) {
		//fout legingegevens
	echo '<p>' . $feedback . '</p>';
	}
}



	$inhoud .= '	<form name="login" method="post" action="' . $_SERVER ["PHP_SELF"] . '">';
	$inhoud .= '	<label for="gebruikersnaam">gebruikersnaam</label>';
	$inhoud .= '	<input type="text" id="gebruikersnaam" name="gebruikersnaam" placeholder="gebruikersnaam">';
	$inhoud .= '	<label for="paswoord">paswoord</label>';
	$inhoud .= '	<input type="password" id="paswoord" name="paswoord">';
	$inhoud .= '	<input type="submit" name="submit" value="login">';
	$inhoud .= '	<p>nog niet geregistreerd? dat kan via <a href="registreer.php">deze link</a></p>';
	$inhoud .= '	</form>';
	
	/***************************************************************************************************/
	/*                              einde form                                                    */
	/***************************************************************************************************/
require_once("includes/template.inc.php");	
?>

