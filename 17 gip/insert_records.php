<?php
    /* session_start();
	print_r($_SESSION);
    if(!isset($_SESSION["gipUser"])){
		//niet ingelogd
			header("Location: login.php");
	}elseif($_SESSION["gipRecht"] != 5){
		//admin heeft rechten 5
		header("Location:)index.php");
		die();
	}  */
	
	require_once("includes/dbconnect.inc.php");
	require_once("includes/functions.inc.php");
			$recordsID = "";
			$naam = "";
			$discipline = "";
			$recordtijden = "";
			
		/* Een bestaand record aanpassen */
		if(isset($_GET["rid"])){
			$recordsID = $_GET["rid"];	
			//query 1 bepaalde record opvragen
			//vraagteken wordt door een waarde vervangen binnen de if
			$qrySelectRecords = 
			"SELECT recordsID, naam, discipline, recordtijden 
			from tblrecords 
			Order by naam ASC";	
			//query voorbereiden : kijken of die correct is
			if($stmt = mysqli_prepare($dbconnect, $qrySelectRecords))	{
				//variabele verbinden aan de parameters (de vraagtekens)
				//door het type van de variabele mee te geven, zit er extra beveiliging ingebouwd
					mysqli_stmt_bind_param($stmt, "i", $recordsID);//b(boolean, i(intiger), s(string)
					//query uitvoeren
					mysqli_stmt_execute($stmt);
					//waarden koppelen aan variabelen
					mysqli_stmt_bind_result($stmt, $naam, $discipline, $recordtijden);
					//waarden van 1 record in de variabelen stoppen
					//mogelijk omdat je zeker bent dat je gegevens hebt van slechts 1 record
					mysqli_stmt_fetch($stmt);
					//resultset sluiten
					mysqli_stmt_close($stmt);
			}
		}
		if ($_SERVER['REQUEST_METHOD'] =="POST") {
			  $recordsID = $_POST["recordsID"];
			$naam = $_POST["naam"];
			$discipline = $_POST["discipline"];
			$recordtijden = $_POST["recordtijden"];
			if (!empty($naam) && !empty($discipline) && !empty($recordtijden)){
				
			
				if(!isset($fout)){
					 
					$naam = prepText($naam); 
					$discipline = prepText($discipline);

						if($recordsID == ""){
						
						 
										$qryInsertRecords = 
					"INSERT INTO tblrecords (naam, discipline, recordtijden)
					VALUES(?, ?, ?)";

	
					if($stmt = mysqli_prepare($dbconnect, $qryInsertRecords)){
						mysqli_stmt_bind_param($stmt, "sid", $naam, $discipline, $recordtijden);
						if(mysqli_stmt_execute($stmt)) {
							// query succesvol uitgevoed
							$feedback = "<p> Toevoegen van het drankje of een snack  was succesvol</p>";
						} else {
							// query niet succesvol uitgevoerd
							$fout = "<p>Het toevoegen van het drankje of een snack is mislukt</p>";
							
							$fout .= mysqli_stmt_error($stmt);
						}
					}
				} else {
					$qryUpdateRecords = 
					"UPDATE tblrecords 
					SET naam = ?, discipline = ?, recordtijden = ?
					WHERE recordsID = ?";
						if($stmt = mysqli_prepare($dbconnect, $qryUpdateRecords)){
						mysqli_stmt_bind_param($stmt, "siis", $naam, $discipline, $recordtijden, $recordsID);
						if(mysqli_stmt_execute($stmt)) {
							// query succesvol uitgevoed
							$feedback = "<p> Het aanpassen van het record was succesvol</p>";
						} else {
							// query niet succesvol uitgevoerd
							$fout = "<p>Het aanpassen van het record is mislukt</p>";
							
							$fout .= mysqli_stmt_error($stmt);
						}
					}

				       }
				}
		} else {
			
			$fout = "<p>U hebt niet alle verplichte velden ingevuld</p>";
		}
	}
			
?>
    

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>insert_records</title>
</head>

<body>
	<form id="addrecords" name="addrecords" method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
    <?php 
		if(isset($fout)){
			echo $fout;
		} elseif(isset($feedback)){
			echo $feedback;
		}
	
	?>
    	<input type="hidden" name="recordsID" value="<?php echo $recordsID;?>">
		<label for="naam">Naam recordhouder</label>
       <input type="text" id="naam" name="naam" placeholder="Naam recordhouder " required value="<?php echo $naam;?>">
		<label for="discipline"> discipline</label>
       <input type="text" id="discipline" name="discipline" placeholder="discipline"  min="1000" max="2015"value="<?php echo $discipline;?>">
		<label for="recordtijden"> recordtijden</label>
       <input type="text" id="recordtijden" name="recordtijden" placeholder="recordtijden" min="1000" max="2015" value="<?php 		echo $recordtijden;?>">
       <input type="submit" id="submit" name="submit" value="Verzenden">
    </form>

</body>
</html>