<?php 
    require_once("includes/dbconnect.inc.php");
	require_once("includes/functions.inc.php");
	$paginatitel = "registreer";
	$inhoud = "";
	$gebruikersnaam = "";
	$email = "";
	$feedback = "";
	$fout = false;
	$succes = false;
	

	/*verwerking formulier*/

	if($_SERVER['REQUEST_METHOD']  =="POST"){
		$gebruikersnaam = $_POST["gebruikersnaam"];
		$email = $_POST["email"];
		$paswoord = $_POST["paswoord"];
		$paswoordb = $_POST["paswoordb"];
		
		$arrVereist = array("gebruikersnaam", "email", "paswoord","paswoordb");
		foreach ($arrVereist as $veld) {
			if(empty($_POST[$veld])){
				$fout = true;
				$feedback .= $veld . " is een verplicht veld. <br> ";
				 
			}
		} 
		
		if($fout) {
			//niet alle verpichte velden waren ingevuld
			$feedback .= "niet alle verplichte velden zijn ingevuld<br>";
		}else {
			
			//alle verplichte velden waren ingevuld
			//controleren of de gebruikdersnaam aan de voorwaarden voldoet
			$gebruikersnaam = prepUser($gebruikersnaam);
			if($gebruikersnaam == "kort" ) {
				//gebruikersnaam te kort 
				$fout = true;
				$gebruikersnaam = "";
				$feedback .= "uw gebruikersnaam is te kort : minimum 5 karakters<br>";
			}
			
			//controleren of het passwoord aan de voorwaarden voldoet
			$paswoord = prepPass($paswoord, $paswoordb);
			if($paswoord == "kort") {
				$fout = true;
				$feedback .= "uw paswoord is te kort : minimum 8 karakters <br>";
			} elseif ($paswoord == "fout") {
				$fout = true;
				$feedback .= "de paswoorden komen niet overeen<br>";
				
			
			}
			
			// controleren of er zoch geen fouten hebben voorgedaan (paswoord of gebruikersnaam)
			if(!$fout) {
				//alles in orde 
				$qryCheckUser = "SELECT gebruikersID FROM tblgebruikers 
										WHERE gebruikersnaam = LIKE ?";
				if ($stmt = mysqli_prepare($dbconnect, $qryCheckUser)) {
					mysqli_stmt_bind_param($stmt, "s", $gebruikernaam);
					mysqli_stmt_execute($stmt);
					mysqli_stmt_bind_result($stmt, $gebruikersnaamID);
					mysqli_stmt_fetch($stmt);
					mysqli_stmt_close($stmt);
				}
				
				if (!empty($gebruikersID)){
					//$gebruikersID bestaat -. gebruikersnaam in gebruik
					$feedback .= "er bestaat reeds een gebruiker met de door u hekozen gebruikersnaam<br>";
					$gebruikers = "";
					$fout = true;
				}else {
					
					//gebruikersnaam bestaat niet -. toevoegen aan de DB
					$qryInsertUser = "INSERT INTO tblgebruikers (gebruikersnaam, paswoord, email) VALUES (?, ?, ?)";
					if($stmt = mysqli_prepare($dbconnect, $qryInsertUser)){

						mysqli_stmt_bind_param($stmt, "sss", $gebruikersnaam, $paswoord, $email);  
						if (mysqli_stmt_execute($stmt)){
						//toevoegen is gelukt
						$feedback .= "u bent succesvol geregistreerd<br>";
					} else {
						//toevoegen is mislukt
						$feedback .= "er heeft zich een fout voorgedaan<br>";
						$fout = true;
						
						}
					
					
				mysqli_stmt_close($stmt);
				}
			}
		}
			
		}
		
	} 
	mysqli_close($dbconnect);
	
	
	
	/*einde verwerking*/
	
	
	
	
	/*Opbouw registratieformulier*/
	
	if($succes) {
		$inhoud.= $feedback;
		$inhoud .= 'U kan nu inloggen via ons  <a href="partiez_login.php">loginformulier</a>';
	}else {
		$inhoud.= $feedback;
	
    $inhoud .= '<form name="registratie" id="registratie" method="post" action="' . $_SERVER['PHP_SELF'] . '">';
	$inhoud .= '<label for="gebruikersnaam">gebruikersnaam</label>';
    $inhoud .= '<input type="text" name="gebruikersnaam" id="gebruikersnaam" value="' .  $gebruikersnaam . ' ">';
    $inhoud .= '<label for="email">uw e-email</label>';
    $inhoud .= '<input type="email" name="email" id="email" value=" ' .  $email . '">';
    $inhoud .= '<label for="password">passwoord</label>';
    $inhoud .= '<input type="password" name="paswoord" id="paswoord">';
    $inhoud .= '<label for="paswoordb">bevesteging paswoord</form>';
    $inhoud .= '<input type="password" name="paswoordb" id="paswoordb">';
    $inhoud .= '<input type="submit" id="submit" value="registreer"> ';
    $inhoud .= '</form>';
	}
	

	/*einde registratieformulier*/
	
	
	
	
	require_once ("includes/template.inc.php");
?>
 